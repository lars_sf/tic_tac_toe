angular.module('tttApp',[])
    .controller('tttController',['$scope', function($scope) {
        $scope.board = [
            [
                {'id': "", 'taken' : false, 'owner': ""},
                {'id': "", 'taken' : false, 'owner': ""},
                {'id': "", 'taken' : false, 'owner': ""}
            ],
            [
                {'id': "", 'taken' : false, 'owner': ""},
                {'id': "", 'taken' : false, 'owner': ""},
                {'id': "", 'taken' : false, 'owner': ""}
            ],
            [
                {'id': "", 'taken' : false, 'owner': ""},
                {'id': "", 'taken' : false, 'owner': ""},
                {'id': "", 'taken' : false, 'owner': ""}
            ]
        ];

        var win_combos = [
            [{'r': 0, 'c':0}, {'r': 0, 'c':1}, {'r': 0, 'c':2}],
            [{'r': 0, 'c':0}, {'r': 1, 'c':0}, {'r': 2, 'c':0}],
            [{'r': 0, 'c':0}, {'r': 1, 'c':1}, {'r': 2, 'c':2}],
            [{'r': 0, 'c':1}, {'r': 1, 'c':1}, {'r': 2, 'c':1}],
            [{'r': 2, 'c':2}, {'r': 1, 'c':1}, {'r': 2, 'c':0}],
            [{'r': 0, 'c':2}, {'r': 1, 'c':2}, {'r': 2, 'c':2}],
            [{'r': 0, 'c':0}, {'r': 1, 'c':0}, {'r': 2, 'c':0}]
        ];

        var game_over = false;
        var max_moves = 7;

        $scope.draw_game = false;
        $scope.whos_turn = "A";
        $scope.moves = 0;
        $scope.winner = "";

        $scope.report_move = function(sq) {
            if (game_over)
                return;

            $scope.board[sq.r][sq.c].taken = true;
            $scope.board[sq.r][sq.c].owner = $scope.whos_turn;

            $scope.moves++;
            if(did_win()) {
                game_over = true;
                $scope.winner = $scope.whos_turn;
            }
            $scope.whos_turn = whos_next();
            if (($scope.moves > max_moves) && ($scope.winner == "")) {
                $scope.draw_game = true;
            }
        };

        var whos_next = function() {
            return (($scope.whos_turn == "A") ? "B" : "A");
        }

        var did_win = function() {
            for (var i = 0; i < win_combos.length ;  i++) {
                var win_opt = win_combos[i];
                var matches = 0;
                for(var j = 0; j < win_opt.length; j++) {
                    var win_sq = win_opt[j];
                    var opt_sq_owner = $scope.board[win_sq.r][win_sq.c].owner;
                    if (opt_sq_owner == $scope.whos_turn ) {
                        matches++;
                    }
                }
                if (matches == 3) {
                    return true;
                }

            }

            return false;
        }
    }]);